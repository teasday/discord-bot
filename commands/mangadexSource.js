const fetch = require('node-fetch')

module.exports = {
  name: 'source',
  aliases: ['sauce'],
  cooldown: 10,
  disabled: false,

  run: function(msg, args) {
    let hash = ''

    if (args.length > 0) {
      const pattern = new RegExp(/^(?:(?:https:\/\/)?(?:s\d\.|www\.)?mangadex.org\/data\/)?([a-f0-9]+)(?:\/\w+\.(?:jpg|png|gif))?$/i)
      const res = pattern.exec(args[0])
      if (res) {
        hash = res[1]
      } else {
        return msg.reply(`that doesn't look like a hash or a MangaDex image link.`)
      }
    } else {
      const lookbackAmount = 50
      const cachedMessages = msg.channel.messages.cache.last(lookbackAmount+1).map(m => m.content).reverse()
      cachedMessages.shift()
      const pattern = new RegExp(/(?:s\d\.|www\.)?mangadex.org\/data\/([a-f0-9]+)/i)
      for (let message of cachedMessages) {
        const res = pattern.exec(message)
        if (res) {
          hash = res[1]
          break
        }
      }
      if (!hash) {
        return msg.reply(`I couldn't find a MangaDex image link within the last ${lookbackAmount} messages.`)
      }
    }

    return fetch(`https://mangadex.org/api/?type=chapter&hash=${hash}`)
      .then(res => res.json())
      .then(json => {
        if (json.manga_id) {
          const title = []
          if (json.volume) title.push(`Vol. ${json.volume}`)
          if (json.chapter) title.push(`Ch. ${json.chapter}`)
          if (json.title) title.push(json.title)
          if (title.length === 0) title.push('Oneshot')
          return msg.reply(`${title.join(' ')}\nhttps://mangadex.org/title/${json.manga_id}`)
        } else {
          throw new Error()
        }
      })
      .catch(e => {
        return msg.reply(`sorry, something went mysteriously wrong while fetching data.`)
      })
  },
}