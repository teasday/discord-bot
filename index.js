require('dotenv').config()
const fs = require('fs')
const { Client, Collection } = require('discord.js')
const config = require('./config.json')

const client = new Client()
client.commands = new Collection()
const cooldowns = new Collection()

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'))
for (const file of commandFiles) {
  const command = require(`./commands/${file}`)
  if (!command.disabled) {
    client.commands.set(command.name, command)
    if (command.cooldown) {
      cooldowns.set(command.name, new Collection())
    }
  }
}

client.once('ready', () => {
  console.log(`[${new Date().toLocaleString()}] Bot started.`)

  client.on('message', msg => {
    if (!msg.content.startsWith(config.prefix) || msg.author.bot) return

    const args = msg.content.slice(config.prefix.length).split(/ +/)
    const cmdName = args.shift().toLowerCase()
    const command = client.commands.get(cmdName) || client.commands.find(cmd => cmd.aliases?.includes(cmdName))

    if (!command) return

    const now = Date.now()
    const timestamps = cooldowns.get(command.name)
    if (timestamps?.has(msg.author.id)) {
      const expires = timestamps.get(msg.author.id) + command.cooldown*1000
      if (now < expires) {
        const timeLeft = ((expires - now) / 1000).toFixed(0)
        return msg.reply(`wait ${timeLeft} ${timeLeft === '1' ? 'second' : 'seconds'} before trying again.`)
      }
    }

    try {
      if (timestamps) {
        timestamps.set(msg.author.id, now)
        setTimeout(() => timestamps.delete(msg.author.id), command.cooldown*1000)
      }
      return command.run(msg, args, client)
    } catch (e) {
      console.error(e)
      return msg.reply(`the command failed to run.`)
    }
  })
})

client.login(process.env.TOKEN)